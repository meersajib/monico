import React, { Component } from 'react';
import Search from '../Filter/Search';
import Category from '../Filter/Category';
import Price from '../Filter/Price';




class Product extends Component {
    state = {
        searchProduct: '',
        selectCategory: '',
        selectPrice: '',
        product: '',
        arrayOfProducts: [
            {
                "id": 1,
                "imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/M63H24W7JF0-L302-ALTGHOST?wid=1500&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
                "category": "shirt",
                "name": "CHECK PRINT SHIRT",
                "price": 110
            },
            {
                "id": 2,
                "imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/FLGLO4FAL12-BEIBR?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
                "category": "shoe",
                "name": "GLORIA HIGH LOGO SNEAKER",
                "price": 91
            },
            {
                "id": 3,
                "imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/HWVG6216060-TAN?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
                "category": "bag",
                "name": "CATE RIGID BAG",
                "price": 94.5
            },
            {
                "id": 4,
                "imgUrl": "http://guesseu.scene7.com/is/image/GuessEU/WC0001FMSWC-G5?wid=520&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
                "category": "watch",
                "name": "GUESS CONNECT WATCH",
                "price": 438.9
            },
            {
                "id": 5,
                "imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/AW6308VIS03-SAP?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
                "category": "scarf",
                "name": "'70s RETRO GLAM KEFIAH",
                "price": 20
            },
            {
                "id": 6,
                "imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/FLGLO4FAL12-BEIBR?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
                "category": "shoe",
                "name": "GLORIA HIGH LOGO SNEAKER",
                "price": 91
            }
        ],
       
    }

    handleInput = e => {
        this.setState({ searchProduct: e.target.value })
    }

    handleCategory = e => {
        this.setState({ selectCategory: e.target.value })
    }
    handlePrice = e => {
        
    }
    productDetails(product){
        this.setState({
            product: product
        })
    }
    

    

    render() {
    
        let filteredProduct = this.state.arrayOfProducts.filter((product) => {
            if (this.state.searchProduct !== '') {
                return product.name.toLowerCase().includes(this.state.searchProduct.toLocaleLowerCase())
            }
            else if (this.state.selectCategory !== '') {
                return product.category.toLowerCase().includes(this.state.selectCategory.toLocaleLowerCase())
            }
            else {
                return product.price
            }

        })
        const productList = filteredProduct
            .map(product => {
                return (
                    <div data-toggle="modal" data-target="#exampleModal" className="col-4" key={product.id} onClick={()=>this.productDetails(product)}>
                        <div className="card mt-5">
                                <img className="img-fluid product" height="250" width="200" src={product.imgUrl} alt="" />
                                <hr />
                                <div className="card-title">
                                    <h6>{product.name}</h6>
                                </div>
                                <div className="">
                                    <p>{product.category}</p>
                                </div>
                            </div>


                            {/* modal */}
                            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div className="modal-dialog" role="document">
                                    <div className="modal-content">
                                    <div className="modal-body">
                                        <img height="300" width="200" src={this.state.product.imgUrl} alt=""/>
                                        <h5 className="text-center">{this.state.product.name}</h5>
                                        <h6>{this.state.product.price}</h6>
                                        <p>{this.state.product.category}</p>

                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            {/* modal */}
                    </div>
                )
            })
        return (
            <div className="container">
               <div className="row">
               <Search handleInput={this.handleInput} />
                <Category handleCategory={this.handleCategory} />
                <Price handlePrice={this.handlePrice} />
               </div>
                <div className="row text-center">
                    {productList}
                </div>
            </div>
        )
    }
}

export default Product;