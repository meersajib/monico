import React, { Component } from 'react';
import axios from 'axios';

class User extends Component {
    state = { 
        user: null
     }
     componentDidMount(){
         let user_id = this.props.match.params.user_id;
         axios.get('https://jsonplaceholder.typicode.com/photos/' + user_id)
        .then(res => {
            this.setState({
                user: res.data
            })
        })   
     }
    render() { 
        const user = this.state.user ? (
            <div className="card">
            <img src={this.state.user.thumbnailUrl} alt=""/>
            <h3 className="pt-2">Name: {this.state.user.title.slice(0,8)}</h3>
            <p className="px-5 py-2">Job Description: {this.state.user.title}</p>
            </div>
        ) : (
            <h2>No User</h2>
        )
        return ( 
            <div className="row text-center d-flex justify-content-center">
                <div className="col-4">
                {user}
                </div>
            </div>
         );
    }
}
 
export default User;