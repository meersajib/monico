import React from 'react';
import Product from '../Product/Product';
import Carousel from '../Slider/Carousel';
import '../style.css';

const Home = () => {
    return (
        <div>
            <Carousel />
            <Product/>
        </div>
    );
}

export default Home;

