import React, { Component } from 'react';

import { BrowserRouter as Router, Route,Switch } from 'react-router-dom';

import Navbar from './Components/Navbar';
import Home from './Home/Home';
import Team from './Team/Team';
import Contact from './Contact/Contact';
import User from './User/User';
// import ProductDetail from './Product/ProductDetail';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Switch>
          <Route exact path="/" component={Home} />
            <Route path="/team" component={Team} />
            <Route path="/contact" component={Contact} />
            <Route path="/:user_id" component={User} />
            {/* <Route path="/:product_id" component={ProductDetail} /> */}
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
