import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Team extends Component {
    state = {
        photos : []
    }
    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/photos')
        .then(res => {
            this.setState({
                photos : res.data.slice(0,9)
            })
        })
    }
    render(){
        const {photos} = this.state;
        const photoList = photos.map(photo => {
            return (
                    <div className="col-4" key={photo.id}>
                        <Link to={'/'+ photo.id}>
                            <img src={photo.thumbnailUrl} alt=""/>
                            <h3>{photo.title.slice(0,8)}</h3>
                        </Link>
                    </div>
            )
        })
        return(
            <div className="container">
            <div className="row d-flex justify-content-center text-center">
                {photoList}
            </div>
            </div>
        )
    }
}

export default Team;