import React, { Component } from 'react';

const initialState = { 
    name: '',
    email: '',
    message: ''
 }

class Contact extends Component {
    constructor(props){
        super(props)

        this.myForm = React.createRef()
    }
    state = initialState;
     changeHandler = e => {
        this.setState({
            [e.target.name] : e.target.value
        })
     }
     submitHandler = e => {
         e.preventDefault()
         alert('Name: '+this.state.name+'\nEmail: '+this.state.email+'\n Message: '+this.state.message)
         this.myForm.current.reset()
         this.setState({
             ...initialState
         })
     }
    render() { 
        return ( 
            <div className="container">
                <div className="row">
                    <div className="col-8">
                    <form ref={this.myForm} onSubmit={this.submitHandler}>
                        <div className="form-group">
                            <label htmlFor="name">Name :</label>
                            <input
                            className="form-control" 
                            type="text" 
                            placeholder="Your Name" 
                            name="name" id="name" 
                            value={this.state.name} 
                            onChange={this.changeHandler}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email :</label>
                            <input
                            className="form-control" 
                            type="email" 
                            placeholder="Your Email" 
                            name="email" id="email" 
                            value={this.state.email} 
                            onChange={this.changeHandler}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="message">Message :</label>
                            <textarea
                            className="form-control" 
                            type="tex" 
                            placeholder="Your Message" 
                            name="message" id="message" 
                            value={this.state.message} 
                            onChange={this.changeHandler}
                            />
                        </div>
                        <button type="submit" className="btn btn-primary">Send Message</button>
                    </form>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default Contact;