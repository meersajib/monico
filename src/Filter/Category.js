import React from 'react';

const Category = (props) => {
    return (
        <div className="col-4 mt-5">
            <div className="form-group">
                <select className="form-control" value={props.value} onChange={props.handleCategory}>
                    <option value=''>All</option>
                    <option value="shirt">Shirt</option>
                    <option value="shoe">Shoe</option>
                    <option value="bag">Bag</option>
                    <option value="watch">Watch</option>
                    <option value="scarf">Scarf</option>
                </select>
            </div>
        </div>
    );
}

export default Category;