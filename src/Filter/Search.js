import React from 'react';

const Search = (props) => {
    return (
        <div className="col-4 mt-5">
            <div className="form-group">
            <input placeholder="Filter by Name" className="form-control" onChange={props.handleInput} type="text"/>
            </div>
        </div>
    );
}

export default Search;