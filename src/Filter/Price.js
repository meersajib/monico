import React from 'react';

const Price = (props) => {
    return (
        <div className="col-4 mt-5">
            <div className="form-group">
                <select className="form-control" value={props.value} onClick={props.handlePrice}>
                    <option value="">Default</option>
                    <option value="low">Low</option>
                    <option value="high">High</option>
                </select>
            </div>
        </div>
    );
}

export default Price;